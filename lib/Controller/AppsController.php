<?php

namespace OCA\MurenaLauncher\Controller;

use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\JSONResponse;
use OCP\IRequest;
use OCA\MurenaLauncher\Service\AppsService;

class AppsController extends Controller {
	private AppsService $appsService;

	public function __construct(
		$appName,
		IRequest $request,
		AppsService $appsService
	) {
		parent::__construct($appName, $request);
		$this->appsService = $appsService;
	}
	/**
	 * @NoAdminRequired
	 * @return JSONResponse
	 */
	public function getOrder() {
		$response = new JSONResponse();
		$response->setData(array("order" => $this->appsService->getAppOrder()));
		return $response;
	}
	/**
	 * @NoAdminRequired
	 * @return JSONResponse
	 */
	public function index() {
		$response = new JSONResponse();
		$entries = $this->appsService->getAppEntries();
		$response->setData($entries);
		return $response;
	}

	/**
	 * @NoAdminRequired
	 */

	public function updateOrder(string $order) {
		$this->appsService->updateOrder($order);
	}
}
