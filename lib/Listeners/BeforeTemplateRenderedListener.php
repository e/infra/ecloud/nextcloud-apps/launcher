<?php


namespace OCA\MurenaLauncher\Listeners;

use OCP\EventDispatcher\Event;
use OCP\AppFramework\Http\Events\BeforeTemplateRenderedEvent;
use \OCP\EventDispatcher\IEventListener;
use OCP\Util;
use OCP\IUserSession;
use OCA\MurenaLauncher\Service\AppsService;
use OCP\INavigationManager;
use OCP\App\IAppManager;
use OCP\AppFramework\Services\IInitialState;

class BeforeTemplateRenderedListener implements IEventListener {
	private Util $util;
	private IUserSession $userSession;
	private INavigationManager $navigationManager;
	private AppsService $appsService;
	private IAppManager $appManager;
	private IInitialState $initialState;
	private string $appName;

	private const ONLYOFFICE_APP_ID = 'onlyoffice';

	public function __construct($appName, Util $util, IUserSession $userSession, INavigationManager $navigationManager, IAppManager $appManager, AppsService $appsService, IInitialState $initialState) {
		$this->appName = $appName;
		$this->util = $util;
		$this->userSession = $userSession;
		$this->navigationManager = $navigationManager;
		$this->appsService = $appsService;
		$this->appManager = $appManager;
		$this->initialState = $initialState;
	}

	public function handle(Event $event): void {
		if (!($event instanceof BeforeTemplateRenderedEvent)) {
			return;
		}
		if ($this->userSession->isLoggedIn()) {
			if ($this->appManager->isEnabledForUser(self::ONLYOFFICE_APP_ID)) {
				$this->addOnlyOfficeEntriesToNavigation();
				$documentsBaseDirectory = $this->appsService->getDocumentsFolder();
				$this->initialState->provideInitialState('documentsBaseDirectory', $documentsBaseDirectory);
			}
			$entries = $this->appsService->getAppEntries();
			$this->initialState->provideInitialState('entries', $entries);
			
			$this->util->addScript($this->appName, $this->appName . '-main');
			$this->util->addStyle($this->appName, 'main');
		}
	}

	private function addOnlyOfficeEntriesToNavigation() {
		$entries = $this->appsService->getOnlyOfficeEntries();
		foreach ($entries as $entry) {
			$this->navigationManager->add(function () use ($entry) {
				return $entry;
			});
		}
	}
}
