
import Vue from 'vue'
import Launcher from './Launcher.vue'
import { generateUrl } from '@nextcloud/router'
import axios from '@nextcloud/axios'
import { translate as t, translatePlural as n } from '@nextcloud/l10n'
import { loadState } from '@nextcloud/initial-state'

Vue.prototype.$t = t
Vue.prototype.$n = n
Vue.prototype.OC = OC
const APPLICATION_NAME = 'murena_launcher'
const ONLYOFFICE_EXTENSIONS = {
	onlyoffice_docx: '.docx',
	onlyoffice_xlsx: '.xlsx',
	onlyoffice_pptx: '.pptx',
}
const hiddenEntryClass = 'hidden-by-launcher'

/**
 *
 */
function showLauncher() {
	document.getElementById('launcher-menu').style.display = 'flex'
}

/**
 *
 */
function hideLauncher() {
	document.getElementById('launcher-menu').style.display = 'none'
}

/**
 *
 */
function mountLauncherComponent() {
	const launcherContainer = document.querySelector('#launcher-menu')

	if (launcherContainer) {
		const View = Vue.extend(Launcher)
		const launcher = new View({})

		launcher.$mount('#launcher-menu')
		hideLauncher()
	} else {
		window.setTimeout(mountLauncherComponent, 50)
	}
}

/**
 *
 * @param parent
 * @param order
 * @param hidden
 */
function mapMenu(parent, order, hidden) {
	const availableApps = {}
	const liElements = parent.querySelectorAll('li.app-menu-entry')
	const documentsBaseDirectory = loadState(APPLICATION_NAME, 'documentsBaseDirectory', '')
	const onlyOfficeEntries = []

	liElements.forEach(function(liElement) {
		const id = liElement.querySelector('a').getAttribute('href')
		const appId = liElement.getAttribute('data-app-id')
		// If app is to be hidden or
		// if app is an external site
		// remove from navbar(skip the "more apps" menu li)

		if (id.includes('onlyoffice')) {
			const correctedHref = id + '&dir=' + documentsBaseDirectory + '&name=' + t(APPLICATION_NAME, 'untitled') + ONLYOFFICE_EXTENSIONS[appId]
			const entry = {
				href: correctedHref,
				element: liElement,
			}
			onlyOfficeEntries.push(entry)
		}

		if (
			hidden.indexOf(id) > -1
			|| (typeof appId === 'undefined')
			|| (appId === 'dashboard')
			|| (appId === 'murena-dashboard')
			|| (appId === 'photos')
		) {
			liElement.remove()
		} else {
			availableApps[id] = liElement
		}
	})

	// Remove hidden from order array
	order = order.filter(function(e) {
		return !(hidden.indexOf(e) > -1)
	})
	order.forEach(function(href) {
		if (availableApps[href]) {
			parent.prepend(availableApps[href])
		}
	})

	onlyOfficeEntries.forEach(entry => {
		const anchor = entry.element.querySelector('a')
		anchor.href = entry.href
	})
}

// Hides the "More" icon and shows only first n entries
/**
 *
 * @param toShow
 *
 */
function showNavbarEntries(toShow) {
	// Hide more menu entry
	// Hide app entries with index + 1 > toShow
	// Show app entries already hidden if index+1 <= toShow
	const entrySelector = '#header .app-menu .app-menu-entry'
	document.querySelectorAll(entrySelector).forEach((entry, index) => {
		if (toShow < index + 1) {
			entry.classList.add(hiddenEntryClass)
		} else {
			entry.classList.remove(hiddenEntryClass)
		}
	})
}

/**
 *
 * @param event
 */
function updateNavbarOrder(event) {
	const orderChange = event.detail
	if (!orderChange) {
		return
	}
	const oldIndex = orderChange.oldIndex
	const newIndex = orderChange.newIndex
	const menuEntrySelector = '#header .app-menu-main > li.app-menu-entry'
	const entries = document.querySelectorAll(menuEntrySelector)

	const totalEntries = entries.length
	if (oldIndex < 0 || oldIndex > totalEntries - 1) {
		return
	}
	if (newIndex < 0 || newIndex > totalEntries - 1) {
		return
	}

	let firstHiddenIndex = -1
	for (let index = 0; index < totalEntries; index++) {
		if (entries[index].classList.contains(hiddenEntryClass)) {
			firstHiddenIndex = index
			break
		}
	}
	if (newIndex < firstHiddenIndex && oldIndex >= firstHiddenIndex) {
		entries[oldIndex].classList.remove(hiddenEntryClass)
		entries[firstHiddenIndex - 1].classList.add(hiddenEntryClass)
	}
	if (newIndex >= firstHiddenIndex && oldIndex < firstHiddenIndex) {
		entries[firstHiddenIndex].classList.remove(hiddenEntryClass)
		entries[oldIndex].classList.add(hiddenEntryClass)
	}

	const parent = entries[oldIndex].parentNode
	const sibling = entries[newIndex]
	if (oldIndex > newIndex) {
		parent.insertBefore(entries[oldIndex], sibling)
	} else {
		parent.insertBefore(entries[oldIndex], sibling)
	}

}

/**
 *
 * @param el
 * @param attrs
 */
function setAttributes(el, attrs) {
	for (const key in attrs) {
		el.setAttribute(key, attrs[key])
	}
}

window.onload = async (event) => {
	const bodyId = document.querySelector('body').id
	if (bodyId === 'body-user' || bodyId === 'body-settings') {
		const launcherDiv = document.createElement('div')
		launcherDiv.setAttribute('id', 'launcher-menu')
		launcherDiv.style.display = 'none'
		/*
			Append base div to header as appending to body directly not working for some
			reason
		*/
		document.querySelector('#header').appendChild(launcherDiv)

		mountLauncherComponent()
		const moreApps = document.createElement('li')
		const moreAppsElementAttrs = {
			id: 'more-apps',
			class: 'menutoggle',
			'aria-haspopup': 'true',
			'aria-controls': 'navigation',
			'aria-expanded': 'false',
		}
		setAttributes(moreApps, moreAppsElementAttrs)
		moreApps.innerHTML = '<a href="#" aria-label="More apps"><div class="icon-more"></div></a>'

		const appMenuElement = '#header .app-menu-main'
		const appMenu = document.querySelector(appMenuElement)

		if (!appMenu) {
			return
		}
		appMenu.appendChild(moreApps)

		// restore existing order
		const response = await axios.get(generateUrl('/apps/murena_launcher/getOrder'))
		let order = []
		if (Array.isArray(response.data.order)) {
			order = response.data.order.reverse()
		}
		const hidden = []

		mapMenu(appMenu, order, hidden)
		window.dispatchEvent(new Event('resize'))
		showNavbarEntries(7)

		const url = window.location.href
		const slug = url => new URL(url).pathname.match(/[^/]+/g)
		const slugs = slug(url)
		const finalSlug = slugs.reverse()[0]
		if (finalSlug !== 'murena-dashboard') {
			appMenu.style.display = 'flex'
		}

		document.addEventListener('navOrderUpdated', updateNavbarOrder)

		moreApps.addEventListener('click', function(e) {
			e.preventDefault()
			if (!document.querySelector('#launcher-menu').offsetParent) {
				e.stopPropagation()
				showLauncher()
			}
		})

		document.body.addEventListener('click', function(e) {
			if (e.target.closest('#launcher-menu')) {
				return
			}
			if (document.querySelector('#launcher-menu').offsetParent) {
				hideLauncher()
			}
		})

		document.body.addEventListener('keydown', function(e) {
			if (e.keyCode === 27) {
				hideLauncher()
			}
		})

	}
}
