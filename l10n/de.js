OC.L10N.register(
    "murena_launcher",
    {
    "Document" : "Dokument",
    "Spreadsheet" : "Tabellendokument",
    "Presentation" : "Präsentation",
    "The required folder was not found" : "Der erforderte Ordner wurde nicht gefunden",
    "MurenaLauncher" : "MurenaLauncher",
    "Murena Launcher App" : "Murena Launcher App",
    "More Apps" : "Mehr Apps",
    "Less Apps" : "Weniger Apps",
    "Apps" : "Apps",
    "untitled": "unbetitelt"
},
"nplurals=2; plural=n != 1;");
