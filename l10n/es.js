OC.L10N.register(
    "murena_launcher",
    {
    "Document" : "Document",
    "Spreadsheet" : "Full de càlcul",
    "Presentation" : "Presentació",
    "The required folder was not found" : "No s'ha trobat la carpeta necessària",
    "MurenaLauncher" : "MurenaLauncher",
    "Murena Launcher App" : "Murena Launcher App",
    "More Apps" : "Más aplicaciones",
    "Less Apps" : "Menos aplicaciones",
    "Apps" : "Aplicaciones",
    "untitled": "sin título"
},
"nplurals=2; plural=(n != 1);");
