OC.L10N.register(
    "murena_launcher",
    {
    "Document" : "Document",
    "Spreadsheet" : "Spreadsheet",
    "Presentation" : "Presentation",
    "The required folder was not found" : "The required folder was not found",
    "MurenaLauncher" : "MurenaLauncher",
    "Murena Launcher App" : "Murena Launcher App",
    "More Apps" : "More Apps",
    "Less Apps" : "Less Apps",
    "Apps" : "Apps",
    "untitled" : "untitled"
},
"nplurals=2; plural=(n != 1);");
