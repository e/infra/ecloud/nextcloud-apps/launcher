OC.L10N.register(
    "murena_launcher",
    {
    "Document" : "Document",
    "Spreadsheet" : "Full de càlcul",
    "Presentation" : "Presentació",
    "The required folder was not found" : "No s'ha trobat la carpeta necessària",
    "MurenaLauncher" : "MurenaLauncher",
    "Murena Launcher App" : "Murena Launcher Ap",
    "More Apps" : "More Apps",
    "Less Apps" : "Less Apps",
    "Apps" : "Apps"
},
"nplurals=2; plural=n != 1;");
