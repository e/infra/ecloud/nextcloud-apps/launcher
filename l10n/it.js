OC.L10N.register(
    "murena_launcher",
    {
    "Document" : "Documento",
    "Spreadsheet" : "Hoja de cálculo",
    "Presentation" : "Presentación",
    "The required folder was not found" : "No se ha encontrado la carpeta requerida",
    "MurenaLauncher" : "MurenaLauncher",
    "Murena Launcher App" : "Murena Launcher App",
    "More Apps" : "Más aplicaciones",
    "Less Apps" : "Menos aplicaciones",
    "Apps" : "Aplicaciones",
    "untitled" : "sin título"
},
"nplurals=2; plural=(n != 1);");
